// entry point
import "bootstrap/dist/js/bootstrap";
import { getMovies, sliceMovies } from "./functions/slider";
import { getUsers } from "./functions/users";

function loadMovies(movie, resetIndexes) {
  if (resetIndexes) {
    from = 0;
    to = 3;
  }

  // Svuoto lo slider prima di mettere i film nuovi
  // (quelli richiesti attraverso la select)
  slider.innerHTML = "";

  getMovies(movie, function (apiMovies) {
    movies = apiMovies;

    //sliceMovies(movies, from, to).forEach(function (movie) {

    //slider.innerHTML +=
    //  '<div class="col-lg-3"><img class="poster" src="' +
    // movie.image +
    //'"></div>';
    //});

    const slices = sliceMovies(movies, from, to);

    //elelento su cui itero un array
    for (const slice of slices) {
      slider.innerHTML += `
      <div class="col-lg-3">
       ${slice.getPosterTag()}
       </div>
       `;
    }
  });
}

var tbody = document.getElementById("tbody");

getUsers(function (users) {
  users.forEach(function (user) {
    tbody.innerHTML =
      "<tr><td>" +
      user.id +
      "</td>" +
      user.email +
      "</td><td>" +
      user.username +
      "</td></tr>";
  });
});

var slider = document.getElementById("slider");
var from = 0;
var to = 3;

var movies = [];

function handleSliderChange(isNext) {
  if (isNext) {
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  } else {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }

  slider.innerHTML = "";
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML +=
      '<div class="col-lg-3"><img class="poster" src="' +
      movie.image +
      '"></div>';
  });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(e.target.id === "slider-next");
  });
});

document.addEventListener("keydown", function (e) {
  if (e.key === "ArrowRight") {
    handleSliderChange(true);
  } else if (e.key === "ArrowLeft") {
    handleSliderChange(false);
  }
});

// Individuo la select nel documento
var sel = document.querySelector('select[name="movies"]');

// Inizializzo lo slider con i film di Batman (almeno qualcosa ce trovo)
loadMovies("batman");

sel.addEventListener("change", function (e) {
  // Carico i film indicati dall'utente con la select
  loadMovies(e.target.value, true);
});

const o = {
  //k = chiave
  k1: "valore",
  k2: 10,
  chiappa2: {},
};
//ES6
//const key conterrà la chiave che di volta in volta viene analizzata dal for-in
for (const key in o) {
  //o.key = accedo alla proprioetà "key" nell'oggetto "o" NON è corretta perchè la costante key non viene
  //interpellata

  // Accedo alla chiave presente in "key" (la prima volta conterrà "k1", la seocnda "k2"...)
  // all'interno dell'oggetto
  console.log(key, o[key]);
}

//ES5
Object.keys(o).forEach(function (key) {
  console.log(key, o[key]);
});

//In (operator)
const o2 = {
  k1: undefined,
  k2: 10,
};

// Nella console non leggiamo k1 perche è undefined e in booleano è false
// Mentre k2, booleano, è true
//if (o2.k1) {
//console.log("k1 esiste");
//}

//if (o2.k2) {
//console.log("k2 esiste");
//}

// Anche una chiave che non esiste in un oggetto restituisce "undefined", come spesso accade in JS
//quando si tenta di accedere a degli elementi inesistenti

if ("k1" in o) {
  console.log("k1 esiste");
}

if ("k2" in o) {
  console.log("k2 esiste");
}
