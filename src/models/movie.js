export class Movie {
  constructor(id, name, image) {
    this.id = id;
    this.name = name;
    this.image = image;
  }

  getPosterTag() {
    // Backtick permette di interpolare dei valori all'interno della stringa
    // -> il valore da interpolare deve essere declinato all'interno ${}
    return `<img class="poster" ssrc="${this.image}">`;
  }
}
